package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessEntityRepository<Task> {

    void addTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    @NotNull
    List<Task> findProjectTasksById(@NotNull String userId, @NotNull String projectId);

    void deleteProjectTasksById(@NotNull String userId, @NotNull String projectId);

    void deleteProjectTaskById(@NotNull String userId, @NotNull String projectId);

}
