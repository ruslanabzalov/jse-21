package tsc.abzalov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.enumeration.CommandType;

import static org.apache.commons.lang3.StringUtils.isBlank;

public abstract class AbstractCommand {

    @Nullable
    protected IServiceLocator serviceLocator;

    public void setServiceLocator(@Nullable IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String getCommandName();

    @Nullable
    public abstract String getCommandArgument();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract CommandType getCommandType();

    public abstract void execute() throws Exception;

    @Override
    @NotNull
    public String toString() {
        @NotNull final String correctArg = (isBlank(getCommandArgument()))
                ? ": "
                : " [" + getCommandArgument() + "]: ";
        return getCommandName() + correctArg + getDescription();
    }

}
