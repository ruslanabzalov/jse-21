package tsc.abzalov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;

public final class AuthLogoffCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "logoff";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Logoff from existing user.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        Optional.ofNullable(serviceLocator).orElseThrow(CommandInitException::new);
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        System.out.println("LOGOFF\n");
        authService.logoff();
        System.out.println("Successful logoff.");
    }

}
