package tsc.abzalov.tm.command.interaction;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.util.InputUtil;

import java.util.List;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

public final class CommandShowProjectTasks extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "show-project-tasks";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show project tasks.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        Optional.ofNullable(serviceLocator).orElseThrow(CommandInitException::new);
        System.out.println("SHOW PROJECT TASKS\n");
        @NotNull final IProjectTaskService projectTasksService = serviceLocator.getProjectTaskService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (projectTasksService.hasData(currentUserId)) {
            System.out.println("Project");
            @NotNull final String projectId = InputUtil.inputId();
            System.out.println();
            @NotNull final List<Task> tasks = projectTasksService.findProjectTasksById(projectId, currentUserId);
            if (tasks.size() == 0) {
                System.out.println("Tasks list is empty.\n");
                return;
            }
            for (@NotNull final Task task : tasks)
                System.out.println((projectTasksService.indexOf(currentUserId, task) + 1) + ". " + task);
            System.out.println();
            return;
        }
        System.out.println("One of the lists is empty!");
    }

}
