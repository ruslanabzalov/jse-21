package tsc.abzalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;

public final class ProjectDeleteByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-project-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete project by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        Optional.ofNullable(serviceLocator).orElseThrow(CommandInitException::new);
        System.out.println("DELETE PROJECT BY ID\n");
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        final boolean areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            projectService.removeById(currentUserId, inputId());
            System.out.println("Project was successfully deleted.\n");
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
