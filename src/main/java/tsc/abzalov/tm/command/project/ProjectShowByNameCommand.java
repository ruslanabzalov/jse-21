package tsc.abzalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.model.Project;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public final class ProjectShowByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "show-project-by-name";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show project by name.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        Optional.ofNullable(serviceLocator).orElseThrow(CommandInitException::new);
        System.out.println("FIND PROJECT BY NAME\n");
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        final boolean areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @NotNull final String projectName = inputName();
            System.out.println();
            @Nullable final Project project = projectService.findByName(currentUserId, projectName);
            if (!Optional.ofNullable(project).isPresent()) {
                System.out.println("Searched project was not found.\n");
                return;
            }
            System.out.println((projectService.indexOf(currentUserId, project) + 1) + ". " + project + "\n");
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
