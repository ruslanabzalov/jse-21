package tsc.abzalov.tm.command.sorting;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.List;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.SORTING_COMMAND;

public final class SortingProjectsByStartDateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "sort-projects-by-start-date";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sort projects by start date.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SORTING_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        Optional.ofNullable(serviceLocator).orElseThrow(CommandInitException::new);
        System.out.println("ALL PROJECTS LIST SORTED BY START DATE\n");
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        final boolean areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @NotNull final List<Project> projects = projectService.sortByStartDate(currentUserId);
            for (@NotNull final Project project : projects)
                System.out.println((projectService.indexOf(currentUserId, project) + 1) + ". " + project);
            System.out.println();
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
