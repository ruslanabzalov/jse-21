package tsc.abzalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public final class SystemHelpCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "help";
    }

    @Override
    @NotNull
    public String getCommandArgument() {
        return "-h";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Shows all available commands.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        Optional.ofNullable(serviceLocator).orElseThrow(CommandInitException::new);
        @NotNull final List<AbstractCommand> commandsList =
                new ArrayList<>(serviceLocator.getCommandService().getCommands());
        commandsList.sort((first, second) -> {
            final int firstOrdinal = first.getCommandType().ordinal();
            final int secondOrdinal = second.getCommandType().ordinal();
            if (firstOrdinal == secondOrdinal) return 0;
            if (firstOrdinal > secondOrdinal) return firstOrdinal - secondOrdinal;
            return secondOrdinal - firstOrdinal;
        });
        @NotNull String helpMessage = formatHelpMessage(commandsList);
        System.out.print(helpMessage);
    }

    @NotNull
    private String formatHelpMessage(@NotNull List<AbstractCommand> commands) {
        @NotNull final StringBuilder builder = new StringBuilder();
        for (@NotNull final AbstractCommand command : commands)
            builder.append(command.toString()).append("\n");
        return builder.append("\n").toString();
    }

}
