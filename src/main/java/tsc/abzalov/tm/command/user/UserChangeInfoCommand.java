package tsc.abzalov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.model.User;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.USER_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputFirstName;
import static tsc.abzalov.tm.util.InputUtil.inputLastName;

public final class UserChangeInfoCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "change-user-info";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change user info.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return USER_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        Optional.ofNullable(serviceLocator).orElseThrow(CommandInitException::new);
        System.out.println("CHANGE USER INFO");
        @NotNull final String userFirstName = inputFirstName();
        @Nullable final String userLastName = inputLastName();
        @Nullable final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        @Nullable User updatedUser =
                serviceLocator.getUserService().editUserInfo(currentUserId, userFirstName, userLastName);
        Optional.ofNullable(updatedUser).orElseThrow(() -> new UserIsNotExistException(currentUserId));
        System.out.println("User info successfully changed.");
    }

}
