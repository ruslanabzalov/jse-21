package tsc.abzalov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.USER_COMMAND;

public final class UserShowInfoCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "show-user-info";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all user information.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return USER_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        Optional.ofNullable(serviceLocator).orElseThrow(CommandInitException::new);
        System.out.println("SHOW USER INFO");
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println(serviceLocator.getUserService().findById(currentUserId) + "\n");
    }

}
