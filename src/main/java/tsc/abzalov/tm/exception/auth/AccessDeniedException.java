package tsc.abzalov.tm.exception.auth;

import tsc.abzalov.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Access denied! Please, check your password and try again.");
    }

}
