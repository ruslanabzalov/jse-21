package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Status;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.constant.LiteralConst.*;
import static tsc.abzalov.tm.enumeration.Status.TODO;
import static tsc.abzalov.tm.util.Formatter.DATE_TIME_FORMATTER;

public abstract class BusinessEntity extends AbstractEntity {

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private Status status = TODO;

    @Nullable
    private LocalDateTime startDate;

    @Nullable
    private LocalDateTime endDate;

    @NotNull
    private String userId;

    public BusinessEntity(
            @NotNull final String name, @NotNull final String description, @NotNull final String userId
    ) {
        this.name = name;
        this.description = Optional.of(description).orElse(DEFAULT_DESCRIPTION);
        this.userId = userId;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull final String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    @Nullable
    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(@Nullable final LocalDateTime startDate) {
        this.startDate = startDate;
    }

    @Nullable
    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(@Nullable final LocalDateTime endDate) {
        this.endDate = endDate;
    }

    @NotNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NotNull final String userId) {
        this.userId = userId;
    }

    @Override
    @NotNull
    public String toString() {
        // TODO: Придумать что-нибудь с Optional и датами.
        final String correctStartDate = (this.startDate == null)
                ? IS_NOT_STARTED
                : this.startDate.format(DATE_TIME_FORMATTER);
        final String correctEndDate = (this.endDate == null)
                ? IS_NOT_ENDED
                : this.endDate.format(DATE_TIME_FORMATTER);
        return this.name +
                ": [ID: " + getId() +
                "; Description: " + this.description +
                "; Status: " + this.status.getDisplayName() +
                "; Start Date: " + correctStartDate +
                "; End Date: " + correctEndDate +
                "; User ID: " + this.userId + "]";
    }

}
