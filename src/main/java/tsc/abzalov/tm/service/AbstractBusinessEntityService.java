package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.enumeration.Status;
import tsc.abzalov.tm.exception.data.*;
import tsc.abzalov.tm.model.BusinessEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.ObjectUtils.anyNull;
import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.util.InputUtil.isIndexIncorrect;

@SuppressWarnings("ResultOfMethodCallIgnored")
public abstract class AbstractBusinessEntityService<T extends BusinessEntity> extends AbstractService<T>
        implements IBusinessEntityService<T> {

    @NotNull
    private final IBusinessEntityRepository<T> repository;

    public AbstractBusinessEntityService(@NotNull IBusinessEntityRepository<T> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public int size(@NotNull final String userId) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        return repository.size(userId);
    }

    @Override
    public boolean isEmpty(@NotNull final String userId) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        return repository.isEmpty(userId);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int indexOf(@NotNull final String userId, @NotNull final T entity) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(entity).orElseThrow(EmptyEntityException::new);
        return repository.indexOf(userId, entity);
    }

    @Override
    @NotNull
    public List<T> findAll(@NotNull final String userId) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        return repository.findAll(userId);
    }

    @Override
    @Nullable
    public T findById(@NotNull final String userId, @NotNull final String id) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(repository.findById(userId, id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public T findByIndex(@NotNull final String userId, int index) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        index = index - 1;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);
        return Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public T findByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        return Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public T editById(
            @NotNull final String userId, @NotNull final String id,
            @NotNull final String name, @NotNull String description
    ) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        description = Optional.of(description).orElse(DEFAULT_DESCRIPTION);
        return Optional.ofNullable(repository.editById(userId, id, name, description))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public T editByIndex(
            @NotNull final String userId, int index,
            @NotNull final String name, @NotNull String description
    ) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        --index;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        description = Optional.of(description).orElse(DEFAULT_DESCRIPTION);
        return Optional.ofNullable(repository.editByIndex(userId, index, name, description))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public T editByName(
            @NotNull final String userId, @NotNull final String name,
            @NotNull String description
    ) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        description = Optional.of(description).orElse(DEFAULT_DESCRIPTION);
        return Optional.ofNullable(repository.editByName(userId, name, description))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        repository.clear(userId);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        repository.removeById(userId, id);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, int index) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        --index;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);
        repository.removeByIndex(userId, index);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        repository.removeByName(userId, name);
    }

    @Override
    @Nullable
    public T startById(@NotNull final String userId, @NotNull final String id) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(repository.startById(userId, id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public T endById(@NotNull final String userId, @NotNull final String id) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(repository.endById(userId, id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    public List<T> sortByName(@NotNull final String userId) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull final List<T> entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @NotNull final String firstName = first.getName();
            @NotNull final String secondName = second.getName();
            return firstName.compareTo(secondName);
        });
        return entities;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    @NotNull
    public List<T> sortByStartDate(@NotNull final String userId) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull final List<T> entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @Nullable final LocalDateTime firstStartDate = first.getStartDate();
            @Nullable final LocalDateTime secondStartDate = second.getStartDate();
            if (anyNull(firstStartDate, secondStartDate)) return 0;
            return firstStartDate.compareTo(secondStartDate);
        });
        return entities;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    @NotNull
    public List<T> sortByEndDate(@NotNull final String userId) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull final List<T> entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @Nullable final LocalDateTime firstEndDate = first.getEndDate();
            @Nullable final LocalDateTime secondEndDate = second.getEndDate();
            if (anyNull(firstEndDate, secondEndDate)) return 0;
            return firstEndDate.compareTo(secondEndDate);
        });
        return entities;
    }

    @Override
    @NotNull
    public List<T> sortByStatus(@NotNull final String userId) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull final List<T> entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @NotNull final Status firstStatus = first.getStatus();
            @NotNull final Status secondStatus = second.getStatus();
            return firstStatus.compareTo(secondStatus);
        });
        return entities;
    }

}
