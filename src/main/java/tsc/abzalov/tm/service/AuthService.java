package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.exception.auth.*;
import tsc.abzalov.tm.model.User;

import java.util.Optional;

import static tsc.abzalov.tm.util.HashUtil.hash;

@SuppressWarnings("ResultOfMethodCallIgnored")
public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String currentUserId;

    @Nullable
    private String currentUserLogin;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void register(
            @NotNull final String login, @NotNull final String password,
            @NotNull final String firstName, @Nullable final String lastName,
            @NotNull final String email
    ) throws Exception {
        Optional.ofNullable(userService.findByLogin(login)).orElseThrow(UserAlreadyExistsException::new);
        Optional.ofNullable(userService.findByEmail(email)).orElseThrow(UserAlreadyExistsException::new);
        @NotNull final User newUser = new User(login, password, firstName, lastName, email);
        currentUserId = newUser.getId();
        currentUserLogin = login;
    }


    @Override
    public void login(@NotNull final String login, @NotNull final String password) throws Exception {
        Optional.of(login).orElseThrow(IncorrectCredentialsException::new);
        Optional.of(password).orElseThrow(IncorrectCredentialsException::new);
        @Nullable final User user = userService.findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserIsNotExistException::new);
        if (user.getHashedPassword().equals(hash(password))) {
            currentUserId = user.getId();
            currentUserLogin = user.getLogin();
            return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void logoff() throws Exception {
        Optional.ofNullable(currentUserId).orElseThrow(SessionIsInactiveException::new);
        Optional.ofNullable(currentUserLogin).orElseThrow(SessionIsInactiveException::new);
        currentUserId = null;
        currentUserLogin = null;
    }


    @Override
    public boolean isSessionInactive() {
        return currentUserId == null || currentUserLogin == null;
    }

    @Override
    @NotNull
    public String getCurrentUserLogin() throws Exception {
        Optional.ofNullable(currentUserId).orElseThrow(SessionIsInactiveException::new);
        Optional.ofNullable(currentUserLogin).orElseThrow(SessionIsInactiveException::new);
        return currentUserLogin;
    }

    @Override
    @NotNull
    public String getCurrentUserId() throws Exception {
        Optional.ofNullable(currentUserId).orElseThrow(SessionIsInactiveException::new);
        Optional.ofNullable(currentUserLogin).orElseThrow(SessionIsInactiveException::new);
        return currentUserId;
    }

}
