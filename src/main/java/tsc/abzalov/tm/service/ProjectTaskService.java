package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.exception.data.EmptyEntityException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("ResultOfMethodCallIgnored")
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository,
                              @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public int indexOf(@NotNull final String userId, @NotNull final Task task) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(task).orElseThrow(EmptyEntityException::new);
        return taskRepository.indexOf(userId, task);
    }

    @Override
    public boolean hasData(@NotNull final String userId) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        return projectRepository.size(userId) != 0 && taskRepository.size(userId) != 0;
    }

    @Override
    public void addTaskToProjectById(
            @NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId
    ) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(projectId).orElseThrow(EmptyIdException::new);
        Optional.of(taskId).orElseThrow(EmptyIdException::new);
        taskRepository.addTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    @Nullable
    public Project findProjectById(@NotNull final String userId, @NotNull final String id) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(projectRepository.findById(userId, id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public Task findTaskById(@NotNull final String userId, @NotNull final String id) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(taskRepository.findById(userId, id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    public List<Task> findProjectTasksById(
            @NotNull final String userId, @NotNull final String projectId
    ) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(projectId).orElseThrow(EmptyIdException::new);
        return taskRepository.findProjectTasksById(userId, projectId);
    }

    @Override
    public void deleteProjectById(@NotNull final String userId, @NotNull final String id) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        projectRepository.removeById(userId, id);
    }

    @Override
    public void deleteProjectTasksById(
            @NotNull final String userId, @NotNull final String projectId
    ) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(projectId).orElseThrow(EmptyIdException::new);
        taskRepository.deleteProjectTasksById(userId, projectId);
    }

    @Override
    public void deleteProjectTaskById(
            @NotNull final String userId, @NotNull final String projectId
    ) throws Exception {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(projectId).orElseThrow(EmptyIdException::new);
        taskRepository.deleteProjectTaskById(userId, projectId);
    }

}
